package tomcat

import (
	"net/http"
	"log"
	"strconv"
)

var Runar string


func Undeploy(host string, path string, version int) *http.Response {
	// curl http://name:pwd@${host}/manager/text/undeploy?path=${path}&version=${version}
	url := "http://" + host + "/manager/test/undeploy?path=" + path + "&version=" + strconv.Itoa(version)
	response, err := http.Get(url)
	if err != nil {
		log.Fatal("Could not undeploy application", err)
	}

	return response
}