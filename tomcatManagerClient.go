package main

import (
	"os"
	"github.com/codegangsta/cli"
	"bitbucket.org/runarb/tomcatManagerClient/tomcat"
)

func main() {
	app := cli.NewApp()
	app.Name = "tomcat"
	app.Usage = "This is the usage part"
	app.Version = "0.1"

//	app.Commands = []cli.Command {
//		cli.Command {
//			Name: "deploy",
//			Aliases: []string{"d"},
//			Usage: "Deploy a new application to Tomcat. Example: deploy foobar.war",
//			Subcommands: []cli.Command {
//				{
//					Name: "",
//				},
//			},
//			Action: func(c *cli.Context) {
//				tomcat.Undeploy()
//				println("deploy with values: " + c.Args().First())
//			},
//		},
//	}

	app.Commands = []cli.Command {
		cli.Command {
			Name: "undeploy",
			Aliases: []string{"u"},
			Usage: "Undeploy/remove an existing application from Tomcat. Example: undeploy foobar.war 4",
			Subcommands: []cli.Command {
				{
					Name: "",
				},
			},
			Flags: []cli.Flag{
				cli.StringFlag{
					Name: "host",
					Value: "kundewebtest1.medianorge.no:8079",
					Usage: "Host og port hvor management applikasjonen kjører",
				},
				cli.StringFlag{
					Name: "contextPath",
					Value: "/bestilling",
					Usage: "Context path til applikasjonen som skal un-deployes",
				},
				cli.IntFlag{
					Name: "version",
					Usage: "Versjonsnummeret til applikasjonen",
				},
			},
			Action: func(c *cli.Context) {
				response := tomcat.Undeploy(c.String("host"), c.String("contextPath"), c.Int("version"))

				println(response)
				println("deploy with values: " + c.Args().First())
			},
		},
	}

	app.Flags = []cli.Flag {
		cli.StringFlag{
			Name: "host",
			Value: "http[s]://some.host.com[:port]",
			Usage: "Host name (and port) running the tomcat manager",
		},
	}

	app.Run(os.Args)
}

type Config struct {
	host string
	command string
	parameters string
}